import React from 'react';
import renderer from 'react-test-renderer';
import ProductList from './ProductList';

const APIMock = JSON.parse(JSON.stringify(require('./apimock.json')))

const fetchAPI = () => Promise.resolve({
    APIMock
});

test('Render init state of Product List', () => {
    const component = renderer.create(
      <ProductList fetchAPI={fetchAPI} />
    );

    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});