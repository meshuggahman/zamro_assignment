import React, { Component, Fragment } from 'react';
import styled from 'styled-components';

type Product = {
  Toepassing: string,
  salePrice: string,
  manufacturerName: string,
  Hardheid: string,
  grossPrice: string,
  BUP_UOM: string,
  Artikelnummer: string,
  stepQuantity: string,
  BUP_Value: string,
  badges: string,
  uom: string,
  Kleur: string,
  productImage: string,
  Temperatuurgebied: string,
  BUP_Conversion: string,
  minQuantity: string,
  manufacturerImage: string,
  name: string,
  Materiaal: string,
  sku: string,
  Snoerdikte: string,
  'Inwendige diameter': string,
  'Maat volgens AS568': string,
  listPrice: string,
  channel: string,
  display: boolean,
  atp: Object
}

type AddedProduct = {
  active: boolean
} & Product

type ActiveProductsListProps = {
  activeProducts: Array<AddedProduct>
}

type ProductCheckListProps = {
  products: Array<Product>
}


const Wrapper = styled.div`
  display: flex;
  flex-direction: ${props => props.flexDirection};
  width: ${props => props.width};
  margin: ${props => props.margin};
  min-height: ${props => props.minHeight};
  max-height: ${props => props.maxHeight};
`;

const ImageWrapper = styled.div`
  display: inline-block;
`

const blackListKeys = [
  'name', 'productImage', 'badges', 'grossPrice', 'channel', 'display', 'active', 'atp'
];

const ProductComponent = (props: Product) => {
  const { product } = props;
  const {
    badges,
    productImage,
    grossPrice,
    sku
  } = product;

  const badgesArray = badges.split(/\|/);
  
  return <Wrapper flexDirection="column" margin="0 10px">
      <Wrapper flexDirection="column" minHeight="350px">
        <img alt={sku} src={productImage} />
        <h4>{grossPrice}</h4>
        <ImageWrapper>
          {badgesArray.map((badge, index) => {
            return <img alt="badge" key={index} className="badge" src={badge} />
          })}
        </ImageWrapper>
      </Wrapper>
      <Wrapper flexDirection="column">
        {Object.keys(product).map((key, index) => {
          if (blackListKeys.indexOf(key) === -1) {
            const value = product[key];
            if (typeof(value) !== 'object') {
              return !!value ? <span key={index}>{value}</span> : <span key={index}>-</span>;
            } else {
              return null;
            }
          } else {
            return null;
          }
        })}
       </Wrapper>
  </Wrapper>;
}

const ProductComponentWrapper = styled.div`
  width: 25%;

  img.badge {
    max-width: 35px;
    margin-right: 5px;
  }

  img {
    width: 75%;
  }
`;

const ActiveProducts = (props: ActiveProductsListProps) => {
  const { activeProducts } = props;
  return <Fragment>      
      {!!activeProducts && activeProducts.map((product) => {
        if (product.active) {
          return <ProductComponentWrapper key={product.sku}>
            <ProductComponent product={product} />
          </ProductComponentWrapper>
        } else {
          return null;
        }
      })}
    </Fragment>
};


const ProductCheckList = (props: ProductCheckListProps) => {
  const { products, reRenderProducts } = props;

  return <Wrapper flexDirection="column">
    <Wrapper flexDirection="column" minHeight="350px;">
        <h4>Select</h4>
        {products.map((product) => {
        return <Wrapper key={product.sku}>
            <input id="product" name="product" type="checkbox" onClick={reRenderProducts} data-sku={product.sku}/>
            <label>{product.name}</label>
          </Wrapper>
        })}
    </Wrapper>
    <Wrapper flexDirection="column">
      {Object.keys(products[0]).map((key, index) => {
        if (blackListKeys.indexOf(key) === -1) {
          return <span key={index}>{key}:</span>
        } else {
          return null;
        }
      })}
    </Wrapper>
  </Wrapper>
}


type ProductListProps = {
  fetchAPI: Promise<Response>
}

type ProductListState = {
  products: Array<Product>,
  addedProducts: Array<AddedProduct>
}

class ProductList extends Component<ProductListProps, ProductListState> {
  constructor(props: Props) {
    super(props);

    this.state = {
      products: [],
      addedProducts: []
    }
  }
  reRenderProducts = (event) => {
    const { target } = event;
    const { activeProducts } = this.state;

    const newProducts = activeProducts.map((product) => {
      if (product.sku === target.dataset.sku) {
        product.active = target.checked;
        return product;
      } else {
        return product;
      }
    });

    this.setState({
      activeProducts: newProducts
    })
  }
  componentDidMount = () => {
    this.props.fetchAPI('https://www.zamro.nl/actions/ViewProduct-ProductCompare?SKU=115E19,11545A,115E1A,115576')
      .then(res => res.json())
      .then((res) => {
        const { products } = res;
        const activeProducts = products.map((p) => {
          p.active = false;
          return p;
        }).sort((a, b) => {
          return a.name.toLowerCase().localeCompare(b.name.toLowerCase());
        });
        this.setState({
          products,
          activeProducts
        })
      })
  }
  render() {
    const { products, activeProducts } = this.state;

    return !!products.length && <Wrapper>
      <Wrapper width="20%" margin="0 20px">
          <ProductCheckList products={products} reRenderProducts={this.reRenderProducts.bind(this)}/>
      </Wrapper>
        <Wrapper width="80%" margin="0 20px">
          <ActiveProducts activeProducts={activeProducts}/>
        </Wrapper>
    </Wrapper>
  }
}

export default ProductList;
