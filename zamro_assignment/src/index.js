import React from 'react';
import ReactDOM from 'react-dom';
import ProductList from './ProductList';

const fetchAPI = (url: string) => {
    return fetch(url);
  }

ReactDOM.render(<ProductList fetchAPI={fetchAPI} />, document.getElementById('root'));
