## Test for Zamro

I've used `create-react-app` as my basic tool kit for a fast react project. It is not my favorite bacuse it's a pain in the neck
when it comes to custom babel and webpack configs, but it was useful due to time constaints.

I used `flow`, `jest`, `styled components` and various React tricks I like that you'll see in the code.
I apologize in advance in keeping all this in one file (`ProductList.js`), however there is only one statful component there.
The rest are dumb components that are used for rendering.

Everything should work after you do a clone and a basic `yarn`.
To run the basic test use `yarn jest`. I had to add a `.babelrc` override to be able to compile jest files to actually run them.

Also, it was a surprise for me to discover that `create-react-app` does not treat arrow functions in class properties correctly.
I'd expect that if I declare a class property with arrow function as I did [here](https://bitbucket.org/meshuggahman/zamro_assignment/src/03280648db4fd0fe53c90e8338a4aba481253116/zamro_assignment/src/ProductList.js#lines-200)
it would grab the `this` of the class, but that is not what is happening, so I had to use `bind(this)` in the render.
I think this could be fixed with `babel-plugin-transform-class-properties` but I did not want to mess with `create-react-app` further.

The styles and visual treatment needs work of course, I apologize for not getting it done 100%.

Thank you for the opportunity again! Have a good day!